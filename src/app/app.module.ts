import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { ComponenteunoComponent } from './components/componenteuno/componenteuno.component';
import { ComponentedosComponent } from './components/componentedos/componentedos.component';
import { ComponentetresComponent } from './components/componentetres/componentetres.component';
import { ComponentecuatroComponent } from './components/componentecuatro/componentecuatro.component';

@NgModule({
  declarations: [
    AppComponent,
    ComponenteunoComponent,
    ComponentedosComponent,
    ComponentetresComponent,
    ComponentecuatroComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
