import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-componentecuatro',
  templateUrl: './componentecuatro.component.html',
  styleUrls: ['./componentecuatro.component.css']
})
export class ComponentecuatroComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  arr= [0,1,2,3,4,5,6,7,8,9,10];

  datos=[
        'Gomez Jimenez Steven',
        'Instituto Tecnologico De Cuautla',
        'Grupo: 3'
  ];

  equipos=[
        'America',
        'Guadalajara',
        'Toluca'
  ];

  materias=[
         'IA',
         'Taller De Investigacion',
         'Programacion'
  ];

  banco=[
        'Azteca',
        'Bienestar',
        'HSBS'
  ];
}
