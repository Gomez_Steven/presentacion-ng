import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ComponentecuatroComponent } from './componentecuatro.component';

describe('ComponentecuatroComponent', () => {
  let component: ComponentecuatroComponent;
  let fixture: ComponentFixture<ComponentecuatroComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ComponentecuatroComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ComponentecuatroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
